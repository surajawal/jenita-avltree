#include <stdio.h>
#include <iostream>
#include "avl.hpp"
using namespace std;

#define IS_ROOT 0
#define IS_LEFT 1
#define IS_RIGHT 2

void print(AvlNode*);
void print(AvlNode*, int, int);

// Function to calculate maximum of two numbers
int max(int a, int b) 
{
  return (a > b) ? a : b;
}


// Function to return the height of a tree under that node
int get_height(AvlNode *node)
{
  if (node == NULL)
  {
    return 0;
  }
  else
  {
    return node->height;
  }
}


// Function that helps to rotate a subtree to right with root node
AvlNode* rightRotate(AvlNode *node)
{
  AvlNode *lft = node->left;
  AvlNode *rgt = node->right;

  lft->right = node;
  node->left = rgt;

  node->height = max(get_height(node->left), get_height(node->right)) + 1;
  lft->height = max(get_height(lft->left), get_height(lft->right)) + 1;

  return lft;
}


// Function that helps to rotate a subtree to left with root node
AvlNode* leftRotate(AvlNode *node)
{
  AvlNode *lft = node->left;
  AvlNode *rgt = node->right;

  rgt->left = node;
  node->right = lft;

  node->height = max(get_height(node->left), get_height(node->right)) + 1;
  rgt->height = max(get_height(rgt->left), get_height(rgt->right)) + 1;

  return rgt;
}


// Function to get balance factor of any node
int getBalanceFactor(AvlNode *node)
{
  if (node == NULL)
  {
    return 0;
  }
  else
  {
    return (get_height(node->left) - get_height(node->right));
  }
}


// Function to return the node with minimum element value
AvlNode* minValueNode(AvlNode* node)
{
  AvlNode* current = node;
  while (current->left != NULL)
  {
    current = current->left;
  }
  return current;
}


/**
 * Internal method to insert into a subtree.
 * x is the item to insert.
 * t is the node that roots the subtree.
 * Set the new root of the subtree.
 */
void insert( const int & info, AvlNode * & root )
{
  if (root == NULL)
    root = new AvlNode (info, NULL, NULL);
  else if (info < root->element){
    // for now, even numbers to the left ... [CHANGE THIS]
    insert( info, root->left );
  } else {
    // and off numbers to the right [CHANGE THIS]
    insert( info, root->right );
  }

  root->height = max(get_height(root->left), get_height(root->right)) + 1;

  int balance_factor = getBalanceFactor(root);

  if (balance_factor > 1)
  {
    if (info < root->left->element)
    {
      root = rightRotate(root);
    }
    else if (info > root->left->element)
    {
      root->left = leftRotate(root->left);
      root = rightRotate(root);
    }
  }
  else if (balance_factor < -1)
  {
    if (info > root->right->element)
    {
      root = leftRotate(root);
    }
    else if (info < root->right->element)
    {
      root->right = rightRotate(root->right);
      root = leftRotate(root);
    }
  }
}

/**
 * Internal method to remove from a subtree.
 * info is the item to remove.
 * root is the node that roots the subtree.
 * Set the new root of the subtree.
 */

void remove( const int & info, AvlNode * & root)
{
  if (info < root->element)
  {
    remove(info, root->left);
  }
  else if (info > root->element)
  {
    remove(info, root->right);
  }
  else
  {
    if ((root->left == NULL) && (root->right == NULL))
    {
      AvlNode *null = NULL;
      root = null;
    }
    else if (root->left == NULL)
    {
      AvlNode *temp = root->right;
      AvlNode *null = NULL;
      root->right = null;
      root = temp;
    }
    else if (root->right == NULL)
    {
      AvlNode *temp = root->left;
      AvlNode *null = NULL;
      root->left = null;
      root = temp;
    }
    else
    {
      AvlNode *temp = minValueNode(root->right);
      root->element = temp->element;
      remove(temp->element, root->right);
    }
  }
}

/*
 * You will probably neesd auxiliary mathods to 
 *  - find the minimum of tree
 *  - rotate (single and double, in both directions
 *  - balance am AVL  tree
 */

/*
 * Print methods, do not change
 */
void print(AvlNode *root, int level, int type) {
  if (root == NULL) {
    return;
  }
  if (type == IS_ROOT) {
    std::cout << root -> element << "\n";
  } else {
    for (int i = 1; i < level; i++) {
      std::cout << "| ";
    }
    if (type == IS_LEFT) {
      std::cout << "|l_" << root -> element << "\n";
    } else {
      std::cout << "|r_" << root -> element << "\n";
    }
  }
  if (root -> left != NULL) {
    print(root -> left, level + 1, IS_LEFT);
  }
  if (root -> right != NULL) {
    print(root -> right, level + 1, IS_RIGHT);
  }
}
void print(AvlNode *root) {
  print(root, 0, IS_ROOT);
}
/*
 * END Print methods, do not change
 */


/* 
 * Main method, do not change
 */
int main(int argc, const char * argv[]) {
  AvlNode *root = NULL;
  std::string filename = argv[1];
  freopen(filename.c_str(), "r", stdin);
  std::string input;
  int data;
  while(std::cin >> input){
    if (input == "insert"){
      std::cin>>data;
      insert(data, root);
    } else if(input == "delete"){
      std::cin>>data;
      remove(data, root);
    } else if(input == "print"){
      print(root);
      std::cout << "\n";
    } else{
      std::cout<<"Data not consistent in file";
      break;
    }
  }
  return 0;
}
/*
 * END main method
 */
